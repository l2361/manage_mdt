﻿using ImExMasterClinic.DataAccess;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ManageMasterData
{
    public partial class clinic_FG : System.Web.UI.Page
    {
        private MasterDAO dao = new MasterDAO();
        protected void Page_Load(object sender, EventArgs e)
        {
                       
                //MasterDAO masterDAO = new MasterDAO();
                //DataTable dt = masterDAO.show_log();
                //string x = " ";

                //gvitems.DataSource = dt;
                //gvitems.DataBind();
                       
        }
        protected void SP_select(object sender, EventArgs e)
        {
            
                String CLINICLCT = clinic_lct.Value;
                String PERIOD = peri_od.Value;
                String CLINIC_CODE = clinic_code.Value;
                String LOCATION_FLOOR = location_floor.Value;
                //String CLINIC_TYPE_NAME = clinic_type_name.Value;
                String HASCLINIC = hasclinic_master.Value;


                //String crtd_by = CRTDby.Value;
                MasterDAO masterDAO = new MasterDAO();
                DataTable dt = masterDAO.storedP_select(CLINICLCT, PERIOD, CLINIC_CODE, LOCATION_FLOOR , HASCLINIC/*CLINIC_TYPE_NAME*/);
                string x = " ";

                gvitems.DataSource = dt;
                gvitems.DataBind();
            

        }
        protected void Paging(object sender, GridViewPageEventArgs e)
        {
            if (clinic_lct.Value == "" && peri_od.Value == "" && clinic_code.Value == "" && location_floor.Value == "" && hasclinic_master.Value == "")
            {
                ChangPaging2();
                gvitems.PageIndex = e.NewPageIndex;
                gvitems.DataBind();
            }
            else
            {
                String CLINICLCT = clinic_lct.Value;
                String PERIOD = peri_od.Value;
                String CLINIC_CODE = clinic_code.Value;
                String LOCATION_FLOOR = location_floor.Value;
                //string CLINIC_TYPE_NAME = clinic_type_name.Value;
                String HASCLINIC_MASTER = hasclinic_master.Value;

                ChangPaging(CLINICLCT, PERIOD, CLINIC_CODE, LOCATION_FLOOR, HASCLINIC_MASTER);
                gvitems.PageIndex = e.NewPageIndex;
                gvitems.DataBind();
            }
        }
        protected void ChangPaging(String CLINICLCT, String PERIOD, String CLINIC_CODE, String LOCATION_FLOOR, String HASCLINIC)
        {
            MasterDAO masterDAO = new MasterDAO();
            DataTable dt = masterDAO.storedP_select(CLINICLCT, PERIOD, CLINIC_CODE, LOCATION_FLOOR, HASCLINIC);

            try
            {
                if (dt.Rows.Count > 0)
                {
                    gvitems.DataSource = dt;
                    gvitems.DataBind();
                }
                else
                {
                    gvitems.DataSource = null;
                    gvitems.DataBind();
                }
            }
            catch (Exception ex) { }

        }
        protected void ChangPaging2()
        {
            MasterDAO masterDAO = new MasterDAO();
            DataTable dt = masterDAO.show_log();

            try
            {
                if (dt.Rows.Count > 0)
                {
                    gvitems.DataSource = dt;
                    gvitems.DataBind();
                }
                else
                {
                    gvitems.DataSource = null;
                    gvitems.DataBind();
                }
            }
            catch (Exception ex) { }

        }

        protected void select_clinicG(object sender, EventArgs e)
        {
            String cliniclct = clinic_lct.Value;
            String period = peri_od.Value;
            String cliniccode = clinic_code.Value;
            String floor = location_floor.Value;
            
            MasterDAO masterDAO = new MasterDAO();
            DataTable dt = masterDAO.select_send_clinic_floor_g(cliniclct, period, cliniccode,floor);
            string x = " ";

            gvitems.DataSource = dt;
            gvitems.DataBind();

        }
        
        protected void gvSelRoomTime_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string seq = e.CommandArgument.ToString().Trim();
            
            if (e.CommandName == "edi")
            {
                DataTable edt_data = dao.select_row(seq);
                up_CLINICLCT.Value = edt_data.Rows[0][1].ToString();
                up_PERIOD.Value = edt_data.Rows[0][2].ToString();
                up_CLINICCODE.Value = edt_data.Rows[0][4].ToString();
                up_CLINICNAME.Value = edt_data.Rows[0][5].ToString();
                up_CLINICLO.Value = edt_data.Rows[0][6].ToString();
                up_CLINICLOEN.Value = edt_data.Rows[0][9].ToString();
                up_BUILDINGCODE.Value = edt_data.Rows[0][10].ToString();
                up_BUILDINGNAME.Value = edt_data.Rows[0][11].ToString();
                up_LOCATIONFLOOR.Value = edt_data.Rows[0][13].ToString();
                up_CNGCODE.Value = edt_data.Rows[0][14].ToString();
                up_cng_name.Value = edt_data.Rows[0][15].ToString();
                up_cnd_code.Value = edt_data.Rows[0][18].ToString();
                up_cnd_name.Value = edt_data.Rows[0][19].ToString();
                up_bd_prt.Value = edt_data.Rows[0][22].ToString();
                up_hasclinic.Value = edt_data.Rows[0][3].ToString();
                up_CLINICNAMEn.Value = edt_data.Rows[0][8].ToString();
                up_cnspclty_n.Value = edt_data.Rows[0][21].ToString();
                up_cnspclty_c.Value = edt_data.Rows[0][20].ToString();
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "pop", "openModalEdituser1();", true);

            }
            //else if (e.CommandName == "del")
            //{
            //    DataTable edt_data = dao.select_row(seq);
            //    del1.Text = "คุณต้องการลบ " + edt_data.Rows[0][1].ToString() + " หรือไม่";
            //    Page.ClientScript.RegisterStartupScript(Page.GetType(), "pop", "openModalDeleteuser1();", true);

            //}
            else if (e.CommandName == "dtail")
            {
                if (seq.Length > 0)
                {
                    try
                    {                        

                        DataTable edt_data = dao.select_row(seq);

                        //id.Text = "ID : " + edt_data.Rows[0][0].ToString();
                        CLINICLCT.Text = "CLINIC LCT : " + edt_data.Rows[0][1].ToString();
                        PERIOD.Text = "PERIOD : " + edt_data.Rows[0][2].ToString();
                        CLINICCODE.Text = "CLINIC CODE : " + edt_data.Rows[0][4].ToString();
                        CLINICNAME.Text = "CLINIC NAME : " + edt_data.Rows[0][5].ToString();
                        CLINICLO.Text = "CLINIC LOCATION : " + edt_data.Rows[0][6].ToString();
                        BUIDINGCODE.Text = "BUIDING CODE : " + edt_data.Rows[0][10].ToString();
                        BUIDINGNAME.Text = "BUIDING NAME : " + edt_data.Rows[0][11].ToString();
                        LOFLOOR.Text = "LOCATION FLOOR : " + edt_data.Rows[0][13].ToString();
                        CGCODE.Text = "CLINIC GROUP CODE : " + edt_data.Rows[0][14].ToString();
                        CGNAME.Text = "CLINIC GROUP NAME : " + edt_data.Rows[0][15].ToString();
                        CTCODE.Text = "CLINIC TYPE CODE : " + edt_data.Rows[0][16].ToString();
                        CTNAME.Text = "CLINIC TYPE NAME : " + edt_data.Rows[0][17].ToString();
                        CDCODE.Text = "CLINIC DEPT CODE : " + edt_data.Rows[0][18].ToString();
                        CSCODE.Text = "CLINIC SPCLTY CODE : " + edt_data.Rows[0][20].ToString();
                        CSNAME.Text = "CLINIC SPCLTY NAME : " + edt_data.Rows[0][21].ToString();
                        BPRIORITY.Text = "BUIDING PRIORITY : " + edt_data.Rows[0][22].ToString();
                        BUNURSE.Text = "BUIDING USE NUSE : " + edt_data.Rows[0][23].ToString();
                        CUNURSE.Text = "CLINIC USE NURSE : " + edt_data.Rows[0][25].ToString();
                        POFLAG.Text = "P_OUT_FLAG : " + edt_data.Rows[0][26].ToString();
                        POUTUPDTTM.Text = "P OUT UP DTTM : " + edt_data.Rows[0][27].ToString();
                        CRTDDTTM.Text = "CRTD DTTM : " + edt_data.Rows[0][28].ToString();
                        CRTDDTTM.Text = "CRTD DTTM : " + edt_data.Rows[0][28].ToString();
                        CRTDBY.Text = "crtd by : " + edt_data.Rows[0][29].ToString();
                        BUILDINGNAMEe.Text = "BUILDING NAME EN : " + edt_data.Rows[0][12].ToString();
                        CDNAME.Text = "ClINIC DEPT NAME : " + edt_data.Rows[0][19].ToString();
                        HASCLINIC.Text = "HASCLINIC MASTER : " + edt_data.Rows[0][3].ToString();

                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "pop", "openModalDetailsuser1();", true);
                    }
                    catch (Exception ss)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('พบข้อมูลมีปัญหา')", true);
                    }

                }
                //Page.ClientScript.RegisterStartupScript(Page.GetType(), "pop", "openModalDetailsuser1();", true);

            }
            
        }

        protected void btnEdit_Command(object sender, CommandEventArgs e)
        {
            saveinfo.CommandName = e.CommandArgument.ToString();
        }

        protected void btnUpdPatient_Command(object sender, CommandEventArgs e)
        {
            string id = e.CommandName;
            string clinic_lct = up_CLINICLCT.Value;
            string peri_od = up_PERIOD.Value;
            string clinic_code = up_CLINICCODE.Value;
            string clinic_name = up_CLINICNAME.Value;
            string clinic_lo = up_CLINICLO.Value;
            string clinic_loen = up_CLINICLOEN.Value;
            string building_code = up_BUILDINGCODE.Value;
            string building_name = up_BUILDINGNAME.Value;
            string lo_floor = up_LOCATIONFLOOR.Value;
            string cng_code = up_CNGCODE.Value;
            string cng_name = up_cng_name.Value;
            string cnd_code = up_cnd_code.Value;
            string cnd_name = up_cnd_name.Value;    
            string bd_prt = up_bd_prt.Value;
            string has_clinic = up_hasclinic.Value;
            string clinic_namen = up_CLINICNAMEn.Value;
            string cnspclty_n = up_cnspclty_n.Value;
            string cnspclty_c = up_cnspclty_c.Value;

            DataTable dt = new MasterDAO().updatepatient(id, clinic_lct, peri_od, clinic_code, clinic_name, clinic_lo, clinic_loen, building_code, building_name, lo_floor, cng_code,
            cng_name, cnd_code, cnd_name, bd_prt, has_clinic, clinic_namen, cnspclty_n, cnspclty_c);
        }
        //protected void btnDelete_Command(object sender, CommandEventArgs e)
        //{
        //    saveinfo2.CommandName = e.CommandArgument.ToString();
        //}
        //protected void btnDelPatient_Command(object sender, CommandEventArgs e)
        //{
        //    string id = e.CommandName;
            

        //    DataTable dt = new MasterDAO().deletepatient(id);
        //}
        protected void AddInfo(object sender, EventArgs e)
        {

            String Add_clct = add_clct.Value;
            String Add_period = add_period.Value;
            String Add_cc = add_cc.Value;
            String Add_cn = add_cn.Value;
            String Add_bc = add_bc.Value;
            String Add_lf = add_lf.Value;
            String Add_cgc = add_cgc.Value;
            String Add_ctc = add_ctc.Value;
            String Add_cdc = add_cdc.Value;
            String Add_csc = add_csc.Value;
            String Add_hasclinic = add_hasclinic.Value;
            String Add_cnlo = add_cnlo.Value;
            String Add_cnne = add_cnne.Value;
            String Add_cnlocaten = add_cnlocaten.Value;
            String Add_bn = add_bn.Value;
            String Add_cgn = add_cgn.Value;
            String Add_ctn = add_ctn.Value;
            String Add_cdn = add_cdn.Value;
            String Add_csn = add_csn.Value;
            String Add_bp = add_bp.Value;
            String Add_bun = add_bun.Value;
            String Add_cun = add_cun.Value;
            String Add_poutflag = add_poutflag.Value;

            MasterDAO masterDAO = new MasterDAO();
            DataTable dt = masterDAO.insertclinic(Add_clct, Add_period, Add_cc, Add_cn, Add_bc, Add_lf, Add_cgc, Add_ctc, Add_cdc, Add_csc, Add_hasclinic, Add_cnlo, Add_cnne, Add_cnlocaten, Add_bn, Add_cgn, Add_ctn, Add_cdn, Add_csn, Add_bp, Add_bun, Add_cun, Add_poutflag);


            gvitems.DataSource = dt;
            gvitems.DataBind();

        }




    }
}