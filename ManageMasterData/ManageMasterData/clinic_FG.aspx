﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="clinic_FG.aspx.cs" Inherits="ManageMasterData.clinic_FG" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../chosen/chosen.min.css" rel="stylesheet" />
    <link href="../CSS/site.css?@DateTime.Now.Ticks" rel="stylesheet" />
    <script src="Scripts/jquery-1.9.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="chosen/chosen.jquery.min.js"></script>
    <script src="chosen/chosen.proto.min.js"></script>
    <script src="Scripts/inputmask/inputmask.js"></script>
    <script src="Scripts/inputmask/inputmask.extensions.js"></script>
    <script src="Scripts/inputmask/inputmask.numeric.extensions.js"></script>
    <script src="Scripts/inputmask/inputmask.date.extensions.js"></script>
    <script src="Scripts/inputmask/jquery.inputmask.js"></script>

    <script type="text/javascript"> 
        function openModalEdituser1() {
            $('#editModal').modal('show');
        }
        function openModalDeleteuser1() {
            $('#delModal').modal('show');
        }
        function openModalDetailsuser1() {
            $('#dtailModal').modal('show');
        }
        function openModalAdduser1() {
            $('#addModal').modal('show');
        }
    </script>
    <style>
        .navbar-collapse {
            background-color: black;
            font-size: larger;
        }

        .card-header {
            background-color: lavender;
            padding-bottom: 5px;
            padding-top: 5px;
            padding-left: 15px;
        }

        .card {
            border-radius: 10px;
            background-color: white;
        }

        .button {
            background-color: lavender;
        }

        #row1 {
            margin-bottom: 15px;
            margin-top: 10px;
        }

        #row2 {
            margin-bottom: 15px;
        }

        #row3 {
            margin-bottom: 15px;
        }

        #buttonF {
            display: flex;
            justify-content: center;
            padding-bottom: 25px;
            padding-top: 15px;
        }

        #buttonA {
            display: flex;
            justify-content: end;
            background-color: lavender;
        }

        .row {
            margin-bottom: 15px;
        }



        /*#buttonA {
            display: block;
            width: 80px;
            height: 80px;
            line-height: 80px;
            border: 2px solid #f5f5f5;
            border-radius: 50%;
            color: #f5f5f5;
            text-align: center;
            text-decoration: none;
            background: #555777;
            box-shadow: 0 0 3px gray;
            font-size: 20px;
            font-weight: bold;
        }
        #buttonA:hover{
            background: #777555;
        }*/
    </style>
</head>
<body style="background-color: AliceBlue;">
    <form id="form1" runat="server">
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a runat="server" style="color: grey" href="~/">Home</a></li>
                <li><a runat="server" style="color: grey" href="~/About">About</a></li>
                <li><a runat="server" style="color: grey" href="~/Contact">Contact</a></li>
            </ul>
        </div>
        <div class="container">
            <h1 style="color: darkblue; text-align: center; font-family: 'Angsana New'; font-size: 50px;">CLINIC FLOOR G</h1>
            <div class="row">
                <div class="card">
                    <div class="card-header">
                        <h4>เงื่อนไขการค้นหา</h4>
                    </div>
                    <br />
                    <div class="card-body">
                        <div class="row" id="row1">
                            <div class="col-md-2"></div>
                            <div class="col-sm-2" style="text-align: start">Clinic Lct :</div>
                            <div class="col-sm-2">
                                <input id="clinic_lct" type="text" class="form-control" runat="server" />
                            </div>
                            <div class="col-sm-2" style="text-align: center">
                                Period :
                            </div>
                            <div class="col-sm-2">
                                <select id="peri_od" class="form-control" runat="server">
                                    <option> </option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                            </div>
                        </div>
                        <div class="row" id="row2">
                            <div class="col-md-2"></div>
                            <div class="col-sm-2" style="text-align: start">Clinic Code :</div>
                            <div class="col-sm-2">
                                <input id="clinic_code" type="text" class="form-control" runat="server" />
                            </div>
                            <div class="col-sm-2" style="text-align: center">
                                Floor :
                            </div>
                            <div class="col-sm-2">
                                <select id="location_floor" class="form-control" runat="server">
                                    <option> </option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                </select>
                            </div>
                            <div class="col-md-2"></div>
                        </div>                       
                        <%--<div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-sm-2" style="text-align: start">Clinic Type Name :</div>
                            <div class="col-sm-2">
                            <select id="clinic_type_name" class="form-control" runat="server">
                                    <option> </option>
                                    <option value="ในเวลาราชการ">ในเวลาราชการ</option>
                                    <option value="ทั้งในและนอกเวลาราชการ">ทั้งในและนอกเวลาราชการ</option>
                                    <option value="คลินิคพิเศษนอกเวลาราชการ">คลินิคพิเศษนอกเวลาราชการ</option>
                                </select>
                        </div>
                        </div>--%>
                        <div class="row" id="row3">
                            <div class="col-md-2"></div>
                            <div class="col-sm-2" style="text-align: start">Has Clinic :</div>
                            <div class="col-sm-2">                                
                                <select id="hasclinic_master" class="form-control" runat="server">
                                    <option> </option>
                                    <option value="Y">Y</option>
                                    <option value="N">N</option>
                                </select>
                            </div>
                        </div>
                        <div id="buttonF">
                            <asp:Button ID="btnSubmit" type="submit" class="btn button" Text="ค้นหา" runat="server" OnClick="SP_select" />
                        </div>
                        <div id="buttonA">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModal">
                                เพิ่มข้อมูล
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <asp:GridView ID="gvitems" runat="server" AutoGenerateColumns="false" class="table table-striped table-hover"
                AllowPaging="true" OnPageIndexChanging="Paging" PageSize="4" OnRowCommand="gvSelRoomTime_RowCommand">
                <Columns>
                    <asp:BoundField DataField="id" HeaderText="ID" ItemStyle-Width="50" />
                    <asp:BoundField DataField="CLINICLCT" HeaderText="CLINICLCT" ItemStyle-Width="50" />
                    <asp:BoundField DataField="PERIOD" HeaderText="PERIOD" ItemStyle-Width="10" />
                    <asp:BoundField DataField="CLINIC_CODE" HeaderText="CLINIC CODE" ItemStyle-Width="50" />
                    <asp:BoundField DataField="CLINIC_NAME" HeaderText="CLINIC NAME" ItemStyle-Width="50" />
                    <asp:BoundField DataField="LOCATION_FLOOR" HeaderText="LOCATION FLOOR" ItemStyle-Width="10" />
                    <asp:TemplateField HeaderText="Details" ItemStyle-Width="10">
                        <ItemTemplate>
                            <asp:LinkButton ID="details" CommandArgument='<%#Eval("id")%>' CommandName="dtail" runat="server" ToolTip="รายละเอียด / แก้ไข">
                            <center><i class="glyphicon glyphicon-file" style="font-size:25px;"></i></center>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Edit" ItemStyle-Width="10">
                        <ItemTemplate>
                            <asp:LinkButton ID="edit" CommandArgument='<%#Eval("id")%>' CommandName="edi" OnCommand="btnEdit_Command" runat="server" ToolTip="รายละเอียด / แก้ไข">
                            <center><i class="glyphicon glyphicon-pencil" style="font-size:25px;"></i></center>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:TemplateField HeaderText="Delete" ItemStyle-Width="10">
                        <ItemTemplate>
                            <asp:LinkButton ID="delete" CommandArgument='<%#Eval("id")%>' CommandName="del" OnCommand="btnDelete_Command" runat="server" ToolTip="รายละเอียด / แก้ไข">
                            <center><i class="glyphicon glyphicon-trash" style="font-size:25px;"></i></center>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                </Columns>
                <PagerStyle HorizontalAlign="center" CssClass="pagination-ys" ForeColor="Blue" />
            </asp:GridView>
        </div>
        </div>      
       <%-- ---------------------------------------- Popup Show Details --------------------------------------------------%>
        <div class="modal fade" id="dtailModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>รายละเอียด</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <asp:Label ID="CLINICLCT" runat="server"></asp:Label>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="CLINICCODE" runat="server"></asp:Label>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="CGCODE" runat="server"></asp:Label>
                            </div>                           
                        </div>
                        <div class="row">       
                            <div class="col-md-4">
                                <asp:Label ID="CTCODE" runat="server"></asp:Label>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="CDCODE" runat="server"></asp:Label>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="CSCODE" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <asp:Label ID="BUIDINGCODE" runat="server"></asp:Label>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="BPRIORITY" runat="server"></asp:Label>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="LOFLOOR" runat="server"></asp:Label>
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-md-4">
                                <asp:Label ID="CLINICNAME" runat="server"></asp:Label>
                            </div>   
                            <div class="col-md-4">
                                <asp:Label ID="CLINICLO" runat="server"></asp:Label>
                            </div>
                           <div class="col-md-4">
                                <asp:Label ID="CGNAME" runat="server"></asp:Label>
                            </div>                          
                        </div>
                        <div class="row">      
                            <div class="col-md-4">
                                <asp:Label ID="CTNAME" runat="server"></asp:Label>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="CDNAME" runat="server"></asp:Label>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="CSNAME" runat="server"></asp:Label>
                            </div>                           
                        </div>
                        <div class="row">   
                            <div class="col-md-4">
                                <asp:Label ID="BUIDINGNAME" runat="server"></asp:Label>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="BUILDINGNAMEe" runat="server"></asp:Label>
                            </div>                                                  
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-md-4">
                                <asp:Label ID="CUNURSE" runat="server"></asp:Label>
                            </div>                            
                            <div class="col-md-4">
                                <asp:Label ID="BUNURSE" runat="server"></asp:Label>
                            </div>   
                            <div class="col-md-4">
                                <asp:Label ID="HASCLINIC" runat="server"></asp:Label>
                            </div>                          
                        </div>
                        <div class="row">                                                      
                            <div class="col-md-4">
                                <asp:Label ID="PERIOD" runat="server"></asp:Label>
                            </div>                          
                            <div class="col-md-4">
                                <asp:Label ID="POFLAG" runat="server"></asp:Label>
                            </div>
                        </div>
                        <hr />
                        <div class="row">                          
                            <div class="col-md-4">
                                <asp:Label ID="POUTUPDTTM" runat="server"></asp:Label>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="CRTDDTTM" runat="server"></asp:Label>
                            </div>
                            <div class="col-md-4">
                                <asp:Label ID="CRTDBY" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <%------------------------------------------ Popup Show Edit --------------------------------------------------%>
        <div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4>แก้ไขข้อมูล</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Clinic Lct :</label>
                                <input id="up_CLINICLCT" type="text" class="form-control" placeholder="Clinic LCT" runat="server" />
                            </div>
                            <div class="col-md-4">
                                <label>Clinic Code :</label>
                                <input id="up_CLINICCODE" type="text" class="form-control" placeholder="Clinic LCT" runat="server" />
                            </div>
                            <div class="col-md-4">
                                <label>
                                    Clinic Name :
                                </label>
                                <input id="up_CLINICNAME" type="text" class="form-control" placeholder="Clinic Name" runat="server" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>Clinic Name En :</label>
                                <input id="up_CLINICNAMEn" type="text" class="form-control" placeholder="Clinic Name En" runat="server" />
                            </div>
                            <div class="col-md-4">
                                <label>Clinic Location :</label>
                                <input id="up_CLINICLO" type="text" class="form-control" placeholder="Clinic Location" runat="server" />
                            </div>
                            <div class="col-md-4">
                                <label>
                                    CLINIC LOCAT_EN :
                                </label>
                                <input id="up_CLINICLOEN" type="text" class="form-control" placeholder="CLINIC LOCAT_EN" runat="server" />
                            </div>
                        </div>
                        <div class="row">                          
                            <div class="col-md-4">
                                <label>
                                    Clinic Group Code :
                                </label>
                                <input id="up_CNGCODE" type="text" class="form-control" placeholder="Cn Group Code" runat="server" />
                            </div>
                            <div class="col-md-4">
                                <label>Clinic Group Name :</label>
                                <input id="up_cng_name" type="text" class="form-control" placeholder="Cn Group Name" runat="server" />
                            </div>
                            <div class="col-md-4">
                                <label>Clinic Spclty Name :</label>
                                <input id="up_cnspclty_n" type="text" class="form-control" placeholder="Clinic Spclty Name" runat="server" />
                            </div>
                        </div>
                        <div class="row">                     
                            <div class="col-md-4">
                                <label>
                                    Clinic Dept Code :
                                </label>
                                <input id="up_cnd_code" type="text" class="form-control" placeholder="Cn Dept Code" runat="server" />
                            </div>
                            <div class="col-md-4">
                                <label>Clinic Dept Name :</label>
                                <input id="up_cnd_name" type="text" class="form-control" placeholder="Cn Dept Name" runat="server" />
                            </div>
                            <div class="col-md-4">
                                <label>Clinic Spclty Code :</label>
                                <input id="up_cnspclty_c" type="text" class="form-control" placeholder="Clinic Spclty Code" runat="server" />
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-md-4">
                                <label>Buiding Code :</label>
                                <input id="up_BUILDINGCODE" type="text" class="form-control" placeholder="Buiding Code" runat="server" />
                            </div>
                            <div class="col-md-4">
                                <label>
                                    Buiding Name :
                                </label>
                                <input id="up_BUILDINGNAME" type="text" class="form-control" placeholder="Buiding Name" runat="server" />
                            </div>
                            <div class="col-md-4">
                                <label>
                                    Buiding Priority :
                                </label>
                                <input id="up_bd_prt" type="text" class="form-control" placeholder="Buiding Priority" runat="server" />
                            </div>                         
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-md-4">
                                <label>
                                    Period :
                                </label>
                                <input id="up_PERIOD" type="text" class="form-control" placeholder="Period" runat="server" />
                            </div> 
                            <div class="col-md-4">
                                <label>Location Floor :</label>
                                <input id="up_LOCATIONFLOOR" type="text" class="form-control" placeholder="Location Floor" runat="server" />
                            </div>
                            <div class="col-md-4">
                                <label>Hasclinic Master</label>
                                <select id="up_hasclinic" class="form-control" runat="server">
                                    <option value="N">N</option>
                                    <option value="Y">Y</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="saveinfo" type="button" class="btn btn-secondary" data-bs-dismiss="modal" CommandName="" runat="server" OnCommand="btnUpdPatient_Command" Text="save" />
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <%------------------------------------------ Popup Show Delete --------------------------------------------------%>
        <%--<div class="modal fade" id="delModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <label>ลบข้อมูล</label>
                    </div>
                    <div class="modal-body">
                        <h3>
                            <center>
                                <asp:Label ID="del1" runat="server"></asp:Label></center>
                        </h3>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="saveinfo2" type="button" class="btn btn-secondary" data-bs-dismiss="modal" CommandName="" runat="server" OnCommand="btnDelPatient_Command" Text="Ok" />
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>--%>
        <%------------------------------------------------------- Popup Show AddInformation --------------------------------------------------%>
        <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel">เพิ่มข้อมูล</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Clinic Lct :</label>
                                <input id="add_clct" type="text" class="form-control" placeholder="กรอกตัวเลข" runat="server" />
                            </div>
                            <div class="col-md-4">
                                <label>
                                    Clinic Use Nurse :
                                </label>
                                <select id="add_cun" class="form-control" runat="server">
                                    <option value="N">N</option>
                                    <option value="Y">Y</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label>
                                    Clinic Code :
                                </label>
                                <input id="add_cc" type="text" class="form-control" placeholder="กรอกตัวเลข" runat="server" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>
                                    Clinic Name :
                                </label>
                                <input id="add_cn" type="text" class="form-control" placeholder="กรอกชื่อคลินิค" runat="server" />
                            </div>
                            <div class="col-md-4">
                                <label>
                                    Clinic Location :
                                </label>
                                <input id="add_cnlo" type="text" class="form-control" placeholder="กรอกชื่อที่ตั้งคลินิค" runat="server" />
                            </div>
                            <div class="col-md-4">
                                <label>
                                    Clinic Name En :
                                </label>
                                <input id="add_cnne" type="text" class="form-control" placeholder="กรอกชื่อคลินิค(อังกฤษ)" runat="server" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>
                                    Clinic Locat En :
                                </label>
                                <input id="add_cnlocaten" type="text" class="form-control" placeholder="Floor.xx" runat="server" />
                            </div>
                            <div class="col-md-4">
                                <label>
                                    Clinic Group Code :
                                </label>
                                <input id="add_cgc" type="text" class="form-control" placeholder="กรอกตัวเลข" runat="server" />
                            </div>
                            <div class="col-md-4">
                                <label>
                                    Clinic Group Name :
                                </label>
                                <input id="add_cgn" type="text" class="form-control" placeholder="กรอกชื่อกลุ่มคลินิค" runat="server" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>
                                    Clinic Type Code :
                                </label>
                                <select id="add_ctc" class="form-control" runat="server">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label>
                                    Clinic Type Name :
                                </label>
                                <select id="add_ctn" class="form-control" runat="server">
                                    <option value="ในเวลาราชการ">ในเวลาราชการ</option>
                                    <option value="ทั้งในและนอกเวลาราชการ">ทั้งในและนอกเวลาราชการ</option>
                                    <option value="คลินิคพิเศษนอกเวลาราชการ">คลินิคพิเศษนอกเวลาราชการ</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label>
                                    Clinic Dept Code :
                                </label>
                                <input id="add_cdc" type="text" class="form-control" placeholder="กรอกตัวเลข 10 หลัก" runat="server" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>
                                    Clinic Spclty Code :    
                                </label>
                                <input id="add_csc" type="text" class="form-control" placeholder="กรอกตัวเลข 10 หลัก" runat="server" />
                            </div>
                            <div class="col-md-4">
                                <label>
                                    Clinic Dept Name :
                                </label>
                                <input id="add_cdn" type="text" class="form-control" placeholder="กรอกชื่อ dept" runat="server" />
                            </div>
                            <div class="col-md-4">
                                <label>
                                    Clinic Spclty Name :
                                </label>
                                <input id="add_csn" type="text" class="form-control" placeholder="กรอกชื่อ spclty" runat="server" />
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-md-4">
                                <label>
                                    Location Floor :
                                </label>
                                <select id="add_lf" class="form-control" runat="server">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label>
                                    Building Code :
                                </label>
                                <input id="add_bc" type="text" class="form-control" placeholder="กรอกตัวเลข 2 หลัก" runat="server" />
                            </div>
                            <div class="col-md-4">
                                <label>
                                    Building Name :
                                </label>
                                <input id="add_bn" type="text" class="form-control" placeholder="กรอกชื่อตึก" runat="server" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>
                                    Building Priority :
                                </label>
                                <input id="add_bp" type="text" class="form-control" placeholder="กรอกตัวเลข 1 หรือ 2 หลัก" runat="server" />
                            </div>
                            <div class="col-md-4">
                                <label>
                                    Building Use Nurse :
                                </label>
                                <select id="add_bun" class="form-control" runat="server">
                                    <option value="N">N</option>
                                    <option value="Y">Y</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label>
                                    HASCLINIC MASTER :
                                </label>
                                <select id="add_hasclinic" class="form-control" runat="server">
                                    <option value="N">N</option>
                                    <option value="Y">Y</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label>
                                    Period :
                                </label>
                                <select id="add_period" class="form-control" runat="server">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label>
                                    P Out Flag :
                                </label>
                                <select id="add_poutflag" class="form-control" runat="server">
                                    <option value="0">0</option>
                                    <option value="1">1</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="Button1" type="submit" class="btn button" Text="Save" OnClick="AddInfo" runat="server" />
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

    </form>
</body>
</html>
