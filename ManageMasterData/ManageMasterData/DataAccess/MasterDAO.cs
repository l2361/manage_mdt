﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Globalization;

namespace ImExMasterClinic.DataAccess
{
    public class MasterDAO : DatabaseAccess
    {
        public DataTable storedP_select(string CLINICLCT, string PERIOD, string CLINIC_CODE, string LOCATION_FLOOR,string HASCLINIC)
        {
            DataTable dt = new DataTable();
            string sql = "";
            //sql += "SELECT * from kiosk_master_clinic_floor_G";




            sql = "EXEC sp_select_cn ";
            sql += "@CLINICLCT = '" + CLINICLCT + "'";
            sql += ",@PERIOD = '" + PERIOD + "'";
            sql += ",@CLINIC_CODE = '" + CLINIC_CODE + "'";
            sql += ",@LOCATION_FLOOR = '" + LOCATION_FLOOR + "'";
            //sql += ",@CLINIC_TYPE_NAME = '" + CLINIC_TYPE_NAME + "'";
            sql += ",@HASCLINIC_MASTER = '" + HASCLINIC + "'";


            //sql += ",@crtd_by = '" + crtd_by + "'";
            //sql += "FROM kiosk_master_clinic WHERE active_flag = 'Y' AND clinic_group_seq IS NULL  AND clinic_prefix IS NOT NULL AND clinic_dept_code IS NOT NULL ";
            //sql += "FROM kiosk_master_clinic WHERE active_flag = 'Y' AND clinic_group_seq IS NULL AND clinic_dept_code  != '1021200000'";
            //sql += "order by clinic_building_code";
            return ExecuteDataTableAll(sql);
        }
        public DataTable show_log()
        {
            DataTable dt = new DataTable();
            string sql = "";
            sql += "SELECT * from kiosk_master_clinic_floor_G";




            //sql = "EXEC sp_select ";
            //sql += "@CLINICLCT = '" + cliniclct + "'";
            //sql += ",@PERIOD = '" + period + "'";


            //sql += ",@crtd_by = '" + crtd_by + "'";
            //sql += "FROM kiosk_master_clinic WHERE active_flag = 'Y' AND clinic_group_seq IS NULL  AND clinic_prefix IS NOT NULL AND clinic_dept_code IS NOT NULL ";
            //sql += "FROM kiosk_master_clinic WHERE active_flag = 'Y' AND clinic_group_seq IS NULL AND clinic_dept_code  != '1021200000'";
            //sql += "order by clinic_building_code";
            return ExecuteDataTableAll(sql);
        }
        public DataTable select_send_clinic_floor_g(String cliniclct, String period, String cliniccode,String floor)
        {
            DataTable dt = new DataTable();
            string sql = "";
            //string clinic_code = "";
            sql = "SELECT * FROM kiosk_master_clinic_floor_G WHERE CLINICLCT = '" + cliniclct + "' or PERIOD = '" + period + "' or CLINIC_CODE = '" + cliniccode + "' or LOCATION_FLOOR = '" + floor + "' ";
            //sql += "FROM kiosk_master_clinic WHERE active_flag = 'Y' AND clinic_group_seq IS NULL  AND clinic_prefix IS NOT NULL AND clinic_dept_code IS NOT NULL ";
            //sql += "FROM kiosk_master_clinic WHERE active_flag = 'Y' AND clinic_group_seq IS NULL AND clinic_dept_code  != '1021200000'";
            //sql += "order by clinic_building_code";
            return ExecuteDataTableAll(sql);
        }

        public DataTable updatepatient(string id, string clinic_lct, string peri_od, string clinic_code, string clinic_name, string clinic_lo, string clinic_loen, string building_code, string building_name, string lo_floor, string cng_code,
        string cng_name, string cnd_code, string cnd_name, string bd_prt, string has_clinic, string clinic_namen, string cnspclty_n, string cnspclty_c)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();

            sqlParams.Add(new SqlParameter("@id", id));
            sqlParams.Add(new SqlParameter("@CLINICLCT", clinic_lct));
            sqlParams.Add(new SqlParameter("@PERIOD", peri_od));
            sqlParams.Add(new SqlParameter("@CLINIC_CODE", clinic_code));
            sqlParams.Add(new SqlParameter("@CLINIC_NAME", clinic_name));
            sqlParams.Add(new SqlParameter("@CLINIC_LOCATION", clinic_lo));
            sqlParams.Add(new SqlParameter("@CLINIC_LOCAT_EN", clinic_loen));
            sqlParams.Add(new SqlParameter("@BUILDING_CODE", building_code));
            sqlParams.Add(new SqlParameter("@BUILDING_NAME", building_name));
            sqlParams.Add(new SqlParameter("@LOCATION_FLOOR", lo_floor));
            sqlParams.Add(new SqlParameter("@CLINIC_GROUP_CODE", cng_code));
            sqlParams.Add(new SqlParameter("@CLINIC_GROUP_NAME", cng_name));
            sqlParams.Add(new SqlParameter("@CLINIC_DEPT_CODE", cnd_code));
            sqlParams.Add(new SqlParameter("@CLINIC_DEPT_NAME", cnd_name));
            sqlParams.Add(new SqlParameter("@BUILDING_PRIORITY", bd_prt));
            sqlParams.Add(new SqlParameter("@HASCLINIC_MASTER", has_clinic));
            sqlParams.Add(new SqlParameter("@CLINIC_NAME_EN", clinic_namen));
            sqlParams.Add(new SqlParameter("@CLINIC_SPCLTY_NAME", cnspclty_n));
            sqlParams.Add(new SqlParameter("@CLINIC_SPCLTY_CODE", cnspclty_c));

            return ExecuteDataTable("sp_update_pa", sqlParams);
        }
        //public DataTable deletepatient(string id)
        //{
        //    List<SqlParameter> sqlParams = new List<SqlParameter>();

        //    sqlParams.Add(new SqlParameter("@id", id));


        //    return ExecuteDataTable("sp_delete_cn", sqlParams);
        //}
        public DataTable select_row(string id)
        {
            DataTable dt = new DataTable();
            string sql = "";
            sql += "SELECT * from kiosk_master_clinic_floor_G WHERE id = '" + id + "'";
            return ExecuteDataTableAll(sql);
        }

        //public DataTable insertsp_kiosk_master_clinic_floor_G(String Add_clct, String Add_period, String Add_cc, String Add_cn,String Add_bc, String Add_lf, String Add_cgc, String Add_ctc, String Add_cdc, String Add_csc)
        //{
        //    DataTable dt = new DataTable();
        //    string sql = "";
        //    //string clinic_code = "";
        //    sql = "INSERT INTO [dbo].[kiosk_master_clinic_floor_G]";
        //    sql += "([CLINICLCT],[PERIOD],[CLINIC_CODE],[CLINIC_NAME],[BUILDING_CODE],[LOCATION_FLOOR],[CLINIC_GROUP_CODE],[CLINIC_TYPE_CODE],[CLINIC_DEPT_CODE],[CLINIC_SPCLTY_CODE])";
        //    sql += "VALUES(" + @Add_clct + "," + @Add_period + "," + @Add_cc + "," + Add_cn + ", " + Add_bc + "," + Add_lf + "," + Add_cgc + "," + Add_ctc + "," + Add_cdc + "," + Add_csc + ")";
        //    //sql += "FROM kiosk_master_clinic WHERE active_flag = 'Y' AND clinic_group_seq IS NULL AND clinic_dept_code  != '1021200000'";
        //    //sql += "order by clinic_building_code";
        //    List<SqlParameter> sqlParams = new List<SqlParameter>();
        //    sqlParams.Add(new SqlParameter("@CLINICLCT", Add_clct));
        //    sqlParams.Add(new SqlParameter("@PERIOD", Add_period));
        //    sqlParams.Add(new SqlParameter("@CLINIC_CODE", Add_cc));
        //    sqlParams.Add(new SqlParameter("@CLINIC_NAME", Add_cn));
        //    sqlParams.Add(new SqlParameter("@BUILDING_CODE", Add_bc));
        //    sqlParams.Add(new SqlParameter("@LOCATION_FLOOR", Add_lf));
        //    sqlParams.Add(new SqlParameter("@CLINIC_GROUP_CODE", Add_cgc));
        //    sqlParams.Add(new SqlParameter("@CLINIC_TYPE_CODE", Add_ctc));
        //    sqlParams.Add(new SqlParameter("@CLINIC_DEPT_CODE", Add_cdc));
        //    sqlParams.Add(new SqlParameter("@CLINIC_SPCLTY_CODE", Add_csc));
           
        //    return ExecuteDataTableAll(sql);
        //}
        public DataTable insertclinic(string Add_clct, string Add_period, string Add_cc, string Add_cn, string Add_bc, string Add_lf, string Add_cgc, string Add_ctc, string Add_cdc, string Add_csc, string Add_hasclinic, string Add_cnlo, string Add_cnne, string Add_cnlocaten, string Add_bn, string Add_cgn, string Add_ctn, string Add_cdn, string Add_csn, string Add_bp, string Add_bun, string Add_cun, string Add_poutflag)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();

            
            sqlParams.Add(new SqlParameter("@CLINICLCT", Add_clct));
            sqlParams.Add(new SqlParameter("@PERIOD", Add_period));
            sqlParams.Add(new SqlParameter("@CLINIC_CODE", Add_cc));
            sqlParams.Add(new SqlParameter("@CLINIC_NAME", Add_cn));
            sqlParams.Add(new SqlParameter("@BUILDING_CODE", Add_bc));
            sqlParams.Add(new SqlParameter("@LOCATION_FLOOR", Add_lf));
            sqlParams.Add(new SqlParameter("@CLINIC_GROUP_CODE", Add_cgc));
            sqlParams.Add(new SqlParameter("@CLINIC_TYPE_CODE", Add_ctc));
            sqlParams.Add(new SqlParameter("@CLINIC_DEPT_CODE", Add_cdc));
            sqlParams.Add(new SqlParameter("@CLINIC_SPCLTY_CODE", Add_csc));
            sqlParams.Add(new SqlParameter("@HASCLINIC_MASTER", Add_hasclinic));
            sqlParams.Add(new SqlParameter("@CLINIC_LOCATION", Add_cnlo));
            sqlParams.Add(new SqlParameter("@CLINIC_NAME_EN", Add_cnne));
            sqlParams.Add(new SqlParameter("@CLINIC_LOCAT_EN", Add_cnlocaten));
            sqlParams.Add(new SqlParameter("@BUILDING_NAME", Add_bn));
            sqlParams.Add(new SqlParameter("@CLINIC_GROUP_NAME", Add_cgn));
            sqlParams.Add(new SqlParameter("@CLINIC_TYPE_NAME", Add_ctn));
            sqlParams.Add(new SqlParameter("@CLINIC_DEPT_NAME", Add_cdn));
            sqlParams.Add(new SqlParameter("@CLINIC_SPCLTY_NAME", Add_csn));
            sqlParams.Add(new SqlParameter("@BUILDING_PRIORITY", Add_bp));
            sqlParams.Add(new SqlParameter("@BUILDING_USE_NURSE", Add_bun));
            sqlParams.Add(new SqlParameter("@CLINIC_USE_NURSE", Add_cun));
            sqlParams.Add(new SqlParameter("@P_OUT_FLAG", Add_poutflag));

            sqlParams.Add(new SqlParameter("@crtd_by", "admin"));
            sqlParams.Add(new SqlParameter("@P_OUT_UPD_DTTM", DateTime.Now));
            sqlParams.Add(new SqlParameter("@crtd_dttm", DateTime.Now));
            return ExecuteDataTable("sp_add_cn", sqlParams);
        }


    }
}