﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace ImExMasterClinic.DataAccess
{
    public class DatabaseAccess : IDisposable
    {
        private SqlConnection   Connection;
        private SqlCommand      Command;
        private bool            UseTransaction;

        private string ConnectionString = ConfigurationManager.ConnectionStrings["chulaaccessdatabase"].ConnectionString;
        //private string ConnectionString = new SimpleCrypto().Decrypt(ConfigurationManager.ConnectionStrings["MTClinic"].ConnectionString);
        protected DatabaseAccess()
        {
            Connection = new SqlConnection(ConnectionString);
            //Connection = new SqlConnection(new SimpleCrypto().Decrypt(ConnectionString));
            Command = new SqlCommand();
            Command.Connection = Connection;
        }

        public void BeginTransaction()
        {
            _BeginTransaction(IsolationLevel.ReadCommitted);
        }

        public void BeginTransaction(IsolationLevel isolationLevel)
        {
            _BeginTransaction(isolationLevel);
        }

        private void _BeginTransaction(IsolationLevel isolationLevel)
        {
            if (UseTransaction)
            {
                UseTransaction = false;

                if (Command.Transaction != null)
                    Command.Transaction.Rollback();

                if (Connection.State == ConnectionState.Open)
                    Connection.Close();

                throw new DatabaseAccessException("DatabaseAccess does not support nested transaction.");
            }
            else 
            {
                UseTransaction = true;
                Connection.Open();

                Command.Transaction = Connection.BeginTransaction(isolationLevel);
            }
        }

        public void CommitTransaction()
        {
            if (UseTransaction)
            {
                UseTransaction = false;

                if (Command.Transaction != null)
                    Command.Transaction.Commit();

                Connection.Close();
            }
        }

        public void RollbackTransaction()
        {
            if (UseTransaction)
            {
                UseTransaction = false;

                if (Command.Transaction != null)
                    Command.Transaction.Rollback();
            
                Connection.Close();
            }
        }

        protected DataTable ExecuteDataTable(string storeProcedureName, List<SqlParameter> sqlParams)
        {
            if ((!UseTransaction) && (Connection.State == ConnectionState.Closed))
                Connection.Open();

            DataTable dt = new DataTable();

            Command.CommandText = storeProcedureName;
            Command.CommandType = CommandType.StoredProcedure;

            Command.Parameters.Clear();

            SqlDataAdapter adapter = new SqlDataAdapter(Command);

            for (int i = 0; i < sqlParams.Count; ++i)
            {
                adapter.SelectCommand.Parameters.Add(sqlParams[i]);
            }

            try
            {
                adapter.Fill(dt);
            }
            catch (SqlException ex)
            {
                throw ex;
            }

            if ((!UseTransaction) && (Connection.State == ConnectionState.Open))
                Connection.Close();

            return dt;
        }

        protected DataTable ExecuteDataTableAll(string sql_text)
        {
            if ((!UseTransaction) && (Connection.State == ConnectionState.Closed))
                Connection.Open();

            DataTable dt = new DataTable();

            Command.CommandText = sql_text;
            Command.CommandType = CommandType.Text;

            SqlDataAdapter adapter = new SqlDataAdapter(Command);

            try
            {
                adapter.Fill(dt);
            }
            catch (SqlException ex)
            {
                throw ex;
            }

            if ((!UseTransaction) && (Connection.State == ConnectionState.Open))
                Connection.Close();

            return dt;
        }

        protected DataSet ExecuteDataSet(string storeProcedureName, List<SqlParameter> sqlParams)
        {
            if ((!UseTransaction) && (Connection.State == ConnectionState.Closed))
                Connection.Open();

            DataSet ds = new DataSet();

            Command.CommandText = storeProcedureName;
            Command.CommandType = CommandType.StoredProcedure;

            Command.Parameters.Clear();

            SqlDataAdapter adapter = new SqlDataAdapter(Command);

            for (int i = 0; i < sqlParams.Count; ++i)
            {
                adapter.SelectCommand.Parameters.Add(sqlParams[i]);
            }

            try
            {
                adapter.Fill(ds);
            }
            catch (SqlException ex)
            {
                throw ex;
            }

            if ((!UseTransaction) && (Connection.State == ConnectionState.Open))
                Connection.Close();

            return ds;
        }

        protected object ExecuteScalar(string storeProcedureName, List<SqlParameter> sqlParams)
        {
            if ((!UseTransaction) && (Connection.State == ConnectionState.Closed))
                Connection.Open();

            Command.CommandText = storeProcedureName;
            Command.CommandType = CommandType.StoredProcedure;

            Command.Parameters.Clear();

            foreach (SqlParameter sqlParam in sqlParams)
                Command.Parameters.Add(sqlParam);

            object identity = Command.ExecuteScalar();

            if ((!UseTransaction) && (Connection.State == ConnectionState.Open))
                Connection.Close();

            return identity;
        }

        protected int ExecuteNonQuery(string storeProcedureName, List<SqlParameter> sqlParams)
        {
            if ((!UseTransaction) && (Connection.State == ConnectionState.Closed))
                Connection.Open();

            Command.CommandText = storeProcedureName;
            Command.CommandType = CommandType.StoredProcedure;

            Command.Parameters.Clear();

            foreach (SqlParameter sqlParam in sqlParams)
                Command.Parameters.Add(sqlParam);

            int numRows = Command.ExecuteNonQuery();

            if ((!UseTransaction) && (Connection.State == ConnectionState.Open))
                Connection.Close();

            return numRows;
        }

        protected int ExecuteNonQuerySQL(string sql, List<SqlParameter> sqlParams)
        {
            if ((!UseTransaction) && (Connection.State == ConnectionState.Closed))
                Connection.Open();

            Command.CommandText = sql;
            Command.CommandType = CommandType.Text;

            Command.Parameters.Clear();

            foreach (SqlParameter sqlParam in sqlParams)
                Command.Parameters.Add(sqlParam);

            int numRows = Command.ExecuteNonQuery();

            if ((!UseTransaction) && (Connection.State == ConnectionState.Open))
                Connection.Close();

            return numRows;
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (Connection.State == ConnectionState.Open)
                Connection.Close();
        }

        #endregion
    }

    public class DatabaseAccessException : Exception 
    {
        public DatabaseAccessException(String message) : base(message) 
        {
        }
    }
}